import React from 'react';
import {Image} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const Logo = () => (
    <Image resizeMode= "contain" style={styles.logo} source={require('./logo/qa.png')}/>
);

const styles = EStyleSheet.create({
    logo:{
        height: 150,
        width: 150,
        marginTop: 30,
        marginBottom: 30,
        alignSelf: 'center'
    }
});

export default Logo;
