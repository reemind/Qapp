import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  Text,
  TouchableHighlight
} from 'react-native';

const underlayColor = "#00a8a8";

class PrimaryButton extends React.Component {
    render() {
      return (
      <TouchableHighlight underlayColor={underlayColor} style = {[styles.buttonView, {
          height: 50,
          width: this.props.width,
          marginBottom: 5,
        }]} onPress={this.props.onPress}>
        <Text style = {styles.buttonText}>{this.props.text}</Text>
        </TouchableHighlight>
      );
    }
}

const styles = EStyleSheet.create({
  buttonView: {
      backgroundColor: '$pinkColor',
      justifyContent: 'center',
      borderRadius: 25
  },
  buttonText: {
      color: 'white',
      fontSize: 20,
      textAlign: 'center',
  }
});

module.exports= PrimaryButton;
