import React, {Component} from 'react';
import { Button,Icon} from "native-base";

class MenuButtom extends Component{
  constructor(props) {
    super(props);
  }
  render(){
    return(
        <Button
              transparent
              onPress={() => navigation.openDrawer()}>
              <Icon name="menu" />
        </Button>
    );
  }
}

module.exports= MenuButtom