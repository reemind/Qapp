import React, {Component} from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

class StatusBarBackground extends Component{
  render(){
    return(
      <View style={[styles.statusBarBackground, this.props.style || {}]}> 
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  statusBarBackground: {
    height: (Platform.OS === 'ios') ? 20 :24 , //this is just to test if the platform is iOS to give it a height of 20, else, no height (Android apps have their own status bar)
    backgroundColor: "#01345A",
  }
});

module.exports= StatusBarBackground