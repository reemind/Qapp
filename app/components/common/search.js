import React, {Component} from 'react';
import { SearchBar } from 'react-native-elements';


class HeaderSearch extends Component{
  constructor(props) {
    super(props);
  }
  render(){
    return(
        <SearchBar
        lightTheme
        round
        containerStyle= {{backgroundColor:'#DADDDF',borderBottomWidth: 0,
        }}
        icon={{ type: 'font-awesome', name: 'search' }}
        cancelIcon 
        placeholder='Type Here...' />
    );
  }
}

module.exports= HeaderSearch