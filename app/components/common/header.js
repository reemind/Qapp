import React, {Component} from 'react';
import { Platform } from 'react-native';

import { Header} from 'react-native-elements';


class CommonHeader extends Component{
  constructor(props) {
    super(props);
  }
  render(){
    return(
      <Header
      outerContainerStyles={{ backgroundColor: '#033F6C',
       borderBottomWidth:0,
       shadowOpacity: 1,
          shadowOffset: {
            height: 1,
          },
          shadowRadius: 1,
      height: Platform.OS === 'ios' ? 75 :  75 - 24}}
      leftComponent={{ 
        icon: 'menu', 
        color: '#fff',
        onPress:() => this.props.navigation.openDrawer() }}
      centerComponent={{ text: 'HomeScreen', style: { color: '#fff' } }}
      rightComponent={{ icon: 'home', color: '#fff' }}
    />
    );
  }
}

module.exports= CommonHeader