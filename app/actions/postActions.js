import {
    FETCH_POSTS,
    NEW_POST,
    BaseUrl
} from './types';

export const fetchPosts = () => dispatch => {
            fetch(BaseUrl+'packages', {
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly8xNzIuMTguMTEuMTY6ODAwMC9hcGkvdXNlci9sb2dpbiIsImlhdCI6MTUzNTM0ODI2MSwiZXhwIjoxNTM1MzUxODYxLCJuYmYiOjE1MzUzNDgyNjEsImp0aSI6IjhVb1AwelgwMzZEUkpnNFcifQ.z-O_6QX5pi58r9_pGeZreWm1-_X8H8u0u9wufdjdi0w'
                    },
                }).then(res => res.json())
                .then(
                   res => dispatch({
                                  type: FETCH_POSTS,
                                  payload: res.data
                                })
                  )
                .catch(err => console.log(err));
};

export const createPost = postData => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(postData)
        })
        .then(res => res.json())
        .then(post =>
            dispatch({
                type: NEW_POST,
                payload: post
            })
        );
};
