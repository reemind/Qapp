import {
    FETCH_PROFILE_DATA,
    POST_PROFILE_DATA,
    BaseUrl,
    token
} from './types';

export const fetchProfileData = () => dispatch => {
            fetch(BaseUrl+'profile_data', {
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + token
                    },
                }).then(res => res.json())
                .then(
                   res => dispatch({
                                  type: FETCH_PROFILE_DATA,
                                  payload: res.data
                                })
                  )
                .catch(err => console.log(err));
};


export const postProfileData = postData => dispatch => {
    fetch(BaseUrl+'profile_data', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
        },
      body: JSON.stringify(postData)
    }).then((response) => response.json())
    .then(responseJson => dispatch({
                type: POST_PROFILE_DATA,
                payload: responseJson
              })
    )
    .catch((error) => {
        console.error(error);
    });
      
  };

