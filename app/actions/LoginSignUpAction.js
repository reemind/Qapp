import {
    POST_CREDENTIAL,
    New_User_SignUp,
    BaseUrl
} from './types';


//172.18.11.16

export const usersignup = user_credential => dispatch => {
    debugger;
    fetch(BaseUrl+'user/login', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body:JSON.stringify(user_credential),
    }).then((response) => response.json())
        .then(responseJson => 
                dispatch({
                    type: POST_CREDENTIAL,
                    payload: responseJson
                  })
        )
        .catch((error) => {
            console.error(error);
        });
};

export const new_user_register = new_user_register_data=> dispatch => {
    
    fetch(BaseUrl+'user/app_user_signup', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body:JSON.stringify(new_user_register_data),
    }).then((response) => response.json())
        .then(responseJson => 
                dispatch({
                    type: New_User_SignUp,
                    payload: responseJson
                  })
        )
        .catch((error) => {
            console.error(error);
        });
};