import {
    FETCH_PACKAGES,
    FETCH_PACKAGE_DETAILS,
    BaseUrl,
    token
} from './types';

export const fetchPackagesData = () => dispatch => {
            fetch(BaseUrl+'packages', {
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + token
                    },
                }).then(res => res.json())
                .then(
                   res => dispatch({
                                  type: FETCH_PACKAGES,
                                  payload: res.data
                                })
                  )
                .catch(err => console.log(err));
};

export const fetchPackagesDetails = package_id => dispatch => {
    fetch(BaseUrl+'getpackageswithchapter/'+package_id, {
        method: 'Get',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
        },
    }).then((res) => res.json())
    .then(res => dispatch({
                type: FETCH_PACKAGE_DETAILS,
                payload: res.data
              })
    )
    .catch((error) => {
        console.error(error);
    });
      
  };


