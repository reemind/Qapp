import {
    POST_RECOVERY_EMAIL,
    RECOVERY_EMAIL_VERIFICATION,
    RECOVERY_RESET_PASSWORD,
    BaseUrl
} from './types';



export const  postRecoveryEmail = recovery_email => dispatch => {
    fetch(BaseUrl+'user/app_user_forgot_password', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body:JSON.stringify(recovery_email),
    }).then((response) => response.json())
        .then(responseJson => 
            // console.log(responseJson);
                dispatch({
                    type: POST_RECOVERY_EMAIL,
                    payload: responseJson
                  })
        )
        .catch((error) => {
            console.error(error);
        });
};


export const   recoveryEmailVerification = recovery_email_verification_code => dispatch => {
    fetch(BaseUrl+'user/verification', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body:JSON.stringify(recovery_email_verification_code),
    }).then((response) => response.json())
        .then(responseJson => 
                dispatch({
                    type: RECOVERY_EMAIL_VERIFICATION,
                    payload: responseJson
                  })
        )
        .catch((error) => {
            console.error(error);
        });
};


export const recover_new_reset_password = recover_new_password => dispatch => {
    debugger;
    fetch(BaseUrl+'user/app_forgot_password_reset', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body:JSON.stringify(recover_new_password),
    }).then((response) => response.json())
        .then(responseJson => 
            // console.log(responseJson);
                dispatch({
                    type: RECOVERY_RESET_PASSWORD,
                    payload: responseJson
                  })
        )
        .catch((error) => {
            console.error(error);
        });
};
