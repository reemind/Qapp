import {
 AsyncStorage
} from 'react-native';

export const FETCH_POSTS = 'FETCH_POSTS';
export const NEW_POST = 'NEW_POST';


//password recovery
export const POST_RECOVERY_EMAIL = 'POST_RECOVERY_EMAIL';
export const RECOVERY_EMAIL_VERIFICATION = 'RECOVERY_EMAIL_VERIFICATION';
export const RECOVERY_RESET_PASSWORD = 'RECOVERY_RESET_PASSWORD'

//login and sign-up
export const New_User_SignUp = 'New_User_SignUp';
export const POST_CREDENTIAL = 'POST_CREDENTIAL';

//profile fetch and update
export const FETCH_PROFILE_DATA = 'FETCH_PROFILE_DATA';
export const POST_PROFILE_DATA = 'POST_PROFILE_DATA';

//Package fetch on dashboard
export const FETCH_PACKAGES = 'FETCH_PACKAGES';
export const FETCH_PACKAGE_DETAILS = 'FETCH_PACKAGE_DETAILS';

//export const BaseUrl ='http://172.18.11.16:8000/api/'; // anish ko ipaddress
//export const PublicUrl = 'http://172.18.11.16:8000/';
export const BaseUrl ='http://192.168.100.5:8000/api/';
export const PublicUrl = 'http://192.168.100.5:8000/';



export const token =  "";

