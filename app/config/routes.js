import React, { Component } from 'react';
import {View, Text, Dimensions} from 'react-native';

import {createStackNavigator, createDrawerNavigator,createBottomTabNavigator, TabNavigator} from 'react-navigation';
import Initial from '../screens/initial/initial';
import Home from '../screens/home/home';
import Setting from '../screens/settings/setting';
import Register from '../screens/register/register';
import ForgetPassword from '../screens/forgetPassword/forgetPassword';
import PasswordRecovery from '../screens/PasswordRecovery/PasswordRecovery';
import SideBar from '../screens/sidebar/sidebar';
import filterpage from '../screens/filterpage/filterpage';
import Profile from '../screens/profile/profile';
import QuestionTab from '../screens/QuestionTab/QuestionTab';
// import { Settings } from '../../../../.cache/typescript/2.6/node_modules/@types/react-native';
// import DrawerContent from '../screens/home/DrawerContent'
let { width, height } = Dimensions.get('window');

const HomeStack = createStackNavigator({
    InitialScreen: {
        screen:Initial,
        navigationOptions:{
            header: null,
            gesturesEnabled: false,
        }
    },
    Register: {
        screen:Register,
        navigationOptions:{
            header: null,
            gesturesEnabled: false,
        }
    },
    ForgetPassword: {
        screen:ForgetPassword,
        navigationOptions:{
            header: null,
            gesturesEnabled: false,
        }
    },    
    PasswordRecovery: {
        screen:PasswordRecovery,
        navigationOptions:{
            header: null,
            gesturesEnabled: false,
        }
    }, 
},{
    initialRouteName : 'InitialScreen'
});
 const tabvan= createBottomTabNavigator(
    {
      Home: Home,
      Settings: Setting,
    }
 )

const SidemenuNav = createDrawerNavigator({
    Home: {
      screen: Home,
      navigationOptions:{
        header: null,
        gesturesEnabled: false,
    }
    
    },

    Profile: {
        screen: Profile,
        navigationOptions:{
          header: null,
          gesturesEnabled: false,
      }
      
      },

    Filter: {
        screen:filterpage,
        navigationOptions:{
            header: null,
            gesturesEnabled: false,
        }
    },
    QuestionTab: {
        screen:QuestionTab,
        navigationOptions:{
            header: null,
            gesturesEnabled: false,
        }
    },
    Setting:Setting,

  },{
    contentComponent: SideBar,
    drawerWidth: 0.8 * Dimensions
        .get('window')
        .width,
  });


const RootStack = createStackNavigator(
    {
        HomeScreen: {
            screen:HomeStack,
            navigationOptions:{
                header: null,
                gesturesEnabled: false,
            }
        },
        
        Sidemenu:{
            screen:SidemenuNav,
            navigationOptions:{
                header: null,
                gesturesEnabled: false,
            }
        } ,
      }
);

export default RootStack;
