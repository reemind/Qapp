
import React, { Component } from "react";
import {
  Text, View, Platform, Dimensions, StatusBar,
  ImageBackground, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView,
  Keyboard, TouchableHighlight, AsyncStorage, ScrollView,Image,TouchableOpacity
} from 'react-native';
import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
import { LinearGradient } from 'expo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { Container, Header, Title, Left, Right,Button, Body,Icon, Content, Card, CardItem,Form,
  Label,Input, List, ListItem,Radio } from "native-base";
import styles from './styles';
import { connect} from 'react-redux';
import PropTypes from 'prop-types';
import {fetchProfileData,postProfileData} from '../../actions/ProfileAction';
import Spinner from 'react-native-loading-spinner-overlay';

import { ImagePicker } from 'expo';
import { PublicUrl } from '../../actions/types';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.uploadImage = this.uploadImage.bind(this);
    this.state = {isReady: false};
    this.state = 
      { 
        id :'',
        mobile_no: '',
        full_name: '',
        college_name:'',
        DOB:'',
        email:'',
        street_address:'',
        gender:'',
        profile_image_url:'',
        visible: false,
               };
  }

  static navigationOptions = {
    drawerLabel: 'Profile',
    drawerIcon: ({ tintColor }) => (
      <Icon name='settings' />
    ),
  }

  componentWillMount(){
    this.setState({ visible: true });
    this.props.fetchProfileData();
}
componentWillReceiveProps(nextProps){
  //const BaseUrl = 'http://172.18.11.16:8000/';
  console.log(nextProps.postProfiledata);
  if(typeof nextProps.profile_data!== "undefined"){
    this.setState({
      id : nextProps.profile_data.id,
      full_name:nextProps.profile_data.full_name,
      email:nextProps.profile_data.email,
      mobile_no:nextProps.profile_data.mobile_no,
      street_address:nextProps.profile_data.street_address,
      college_name:nextProps.profile_data.college_name,
      profile_image_url:PublicUrl+nextProps.profile_data.profile_image_url,
      profile_image_data:nextProps.profile_data.profile_image_url,
      DOB:nextProps.profile_data.DOB
  })
    }
    else if(nextProps.postProfiledata.data){
        this.setState({
          id : nextProps.postProfiledata.data.id,
          full_name:nextProps.postProfiledata.data.full_name,
          email:nextProps.postProfiledata.data.email,
          mobile_no:nextProps.postProfiledata.data.mobile_no,
          street_address:nextProps.postProfiledata.data.street_address,
          college_name:nextProps.postProfiledata.data.college_name,
          profile_image_url:PublicUrl+nextProps.postProfiledata.data.profile_image_url,
          profile_image_data:nextProps.postProfiledata.data.profile_image_url,
          DOB:nextProps.postProfiledata.data.DOB
        })
      
  this.setState({ visible: false });

    }
    else{
      this.setState({
        id :'',
        mobile_no: '',
        full_name: '',
        college_name:'',
        DOB:'',
        email:'',
        street_address:'',
        gender:'',
        profile_image_url:'',
        profile_image_data:''
        });
    }

this.setState({ visible: false });
}

  uploadImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      quality :0.5,
      allowsEditing: true,
      aspect: [4, 3],
      base64:true
    });
    if (!result.cancelled) {
      this.setState({ profile_image_url: result.uri });
      this.setState({ profile_image_data: "data:image/jpeg;base64,"+ result.base64.replace(/(\r\n\t|\n|\r\t)/gm,"") });
    }
  };
saveProfile(){
  
  const profileData = {
    full_name: this.state.full_name,
    email: this.state.email,
    mobile_no: this.state.mobile_no,
    street_address: this.state.street_address,
    college_name: this.state.college_name,
    profile_image_url: this.state.profile_image_data,
    DOB: this.state.DOB
  };
  this.props.postProfileData(profileData);
  this.setState({ visible: true });
}


  onCheckBoxPress(ev) {
    if (this.state.value == ev) {
      this.setState({ value: 'Male' });
      return;
    }
    this.setState({ value: 'Female' });
}
  render() {
    let { width, height } = Dimensions.get('window');
        const underlayColor = "#137271";
    return (
      
      <Container>
      <StatusBarBackground></StatusBarBackground>
        <Header style={{backgroundColor:'#033F6C'}}>
          <Left>
          <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" />
        </Button>
          </Left>
          <Body>
            <Title>Profile Page</Title>
          </Body>
          <Right><Button
              transparent
              onPress={() => this.saveProfile()}>
              <Text style={{fontSize:16,color:'white'}}>Save</Text>
        </Button></Right>
        </Header>
        
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{flex: 1}}>
        <KeyboardAwareScrollView
                    innerRef={ref => { this.scroll = ref }}
                    keyboardShouldPersistTaps="handled">
        <ScrollView>
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        <View>
        <View style={{height: 170,
                            flexDirection: 'column',
                            alignItems:'center',
                            backgroundColor:'#DADDDF'}}>
            <TouchableOpacity onPress={() => this.uploadImage()}>
            {this.state.profile_image_url?(
              <Image 
              source={{uri: this.state.profile_image_url}}
            style={{marginTop:0.20*170,marginBottom: 5, height: 100,width:100, borderRadius: 50}}
             />
            ):(
              <Image 
            source={require('../../assets/images/main.jpg')}
            style={{marginTop:0.20*170,marginBottom: 5, height: 100,width:100, borderRadius: 50}}
             />
            )}
            </TouchableOpacity>
           
          <Text style={{
                  color:'#033F6C',
                  fontSize:16
                }}>{this.state.full_name}</Text>
        </View>
        <View>
        <List>
            <ListItem style={styles.listitem}>
                <Icon name="mail"
                style={styles.listIcon}/>
                <Text style={styles.listtext}>Contact Information</Text>
            </ListItem>
             <View>
                <View style={styles.listlable}>
                   <Text style={{fontSize:16, width:0.25*width}}>Email:</Text>
                   <Text style={{fontSize:16}}>{this.state.email}</Text>
                </View>
                <View style={styles.listlable}>
                  <Text style={{marginRight:15,fontSize:16}}>Phone No. :</Text>
                  <Right><TextInput 
                    underlineColorAndroid="transparent"
                    keyboardType='phone-pad'
                    autoCapitalize='none'
                    style={styles.textinput}
                    onChangeText={(mobile_no) => this.setState({mobile_no})}
                    value={this.state.mobile_no}/>
                    </Right>
                </View>

                <View style={styles.listlable}>
                  <Text style={{fontSize:16, width:0.25*width}}>Address:</Text>
                  <Text style={{fontSize:16,paddingRight:10}} numberOfLines={2}>{this.state.street_address}</Text>
                 
                </View>
                <View style={styles.listlable}>
                <Right><Button style={{marginLeft:5, height:25,paddingTop:0,paddingBottom:0,backgroundColor:'#033F6C'}}
                  onPress={() => this.props.navigation.openDrawer()}>
                  <Text style={{fontSize:16,paddingLeft:4,paddingRight:4,color:'white'}}>Use Map</Text>
                  </Button>
                </Right>
                </View>
               
             </View>
            <ListItem style={styles.listitem}>
                <Icon  name="person"
                style={styles.listIcon}/>
                <Text style={styles.listtext}>Details Information</Text>
            </ListItem>
                <View>
            <View style={styles.listlable}>
                  <Text style={{marginRight:15,fontSize:16}}>Full Name:</Text>
                  <Right><TextInput 
                    underlineColorAndroid="transparent"
                    keyboardType='default'
                    autoCapitalize='none'
                    style={styles.textinput}
                    onChangeText={(full_name) => this.setState({full_name})}
                    value={this.state.full_name}/>
                    </Right>
                </View>
                <View style={styles.listlable}>
                  <Text style={{marginRight:15,fontSize:16}}>School Name:</Text>
                  <Right><TextInput 
                    underlineColorAndroid="transparent"
                    keyboardType='default'
                    autoCapitalize='none'
                    style={styles.textinput}
                    onChangeText={(college_name) => this.setState({college_name})}
                    value={this.state.college_name}/>
                    </Right>
                </View>
                <View style={styles.listlable}>
                  <Text style={{marginRight:15,fontSize:16}}>DOB : </Text>
                  <Right><TextInput 
                    underlineColorAndroid="transparent"
                    keyboardType='default'
                    autoCapitalize='none'
                    style={styles.textinput}
                    onChangeText={(DOB) => this.setState({DOB})}
                    value={this.state.DOB}/>
                    </Right>
                </View>
                <View style={styles.listlable}>
                  <Text style={{marginRight:15,fontSize:16}}>Gender : </Text>
                  <Radio
                      style={{
                        width: 24,
                        height: 24,
                        paddingLeft: 3,
                      }}
                     
                    /><Text style={{marginRight:15,marginLeft:10, fontSize:16}}>Male </Text>
                    <Radio
                      style={{
                        width: 24,
                        height: 24,
                        paddingLeft: 3,
                      }}
                     
                    /><Text style={{marginRight:15, marginLeft:10,fontSize:16}}>Female </Text>
                  </View>
                  {/* <Button full rounded primary
                        onPress={() => this.saveProfile()}
                       style={{ marginTop: 10 }}
                   >
                     <Text>Save Profile</Text>
                </Button> */}
              </View>
            </List>
        </View>
        </View>
          </ScrollView>
          </KeyboardAwareScrollView>
       </TouchableWithoutFeedback>
      </Container>
    );
  }

}


Profile.propTypes = {
  fetchProfileData: PropTypes.func.isRequired,
  postProfileData: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
  profile_data: state.profile.profiledata,
  postProfiledata: state.profile.postProfiledata,
});

export default connect(mapStateToProps, {
  fetchProfileData,postProfileData
})(Profile);