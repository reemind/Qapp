import React, { Component } from 'react';
import styles from '../initial/styles';
import PropTypes from 'prop-types';
import messages from '../../data/messages';
import DropdownAlert from 'react-native-dropdownalert';
import validateEmail from '../../helpers/validator';
import { LinearGradient } from 'expo';
import Icon from 'react-native-vector-icons/Ionicons';
import { Text, View, Platform, Dimensions, StatusBar,ImageBackground, TextInput,TouchableWithoutFeedback,
    KeyboardAvoidingView, Keyboard,TouchableHighlight, ScrollView, ModalSelector,Modal} from 'react-native';
import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
import { connect } from 'react-redux';
import { recover_new_reset_password } from '../../actions/PasswordRecoveryAction';

class PasswordRecovery extends Component {

    constructor(props) {
        super(props);
        this.newPasswordSend = this.newPasswordSend.bind(this);
        this.state = {
            new_password: '',
            re_password:'',
            modalVisible: false,
        };
       
    }
    componentWillReceiveProps(nextProps) {
        if(typeof nextProps.recover_new_passwordRes.success !== "undefined"){
            if( nextProps.recover_new_passwordRes.success){
                this.props.navigation.navigate('InitialScreen');
            }
            else{
                this.dropdown.alertWithType('error', 'Alert!', nextProps.recover_new_passwordRes.error);
            } 
        }
    }

    newPasswordSend(e){
        e.preventDefault();
        this.props.navigation.navigate('InitialScreen');
        // const { navigation } = this.props;
        // if(this.state.new_password == this.state.re_password ){
        //     const cred = {
        //         "new_password": this.state.new_password,
        //         "user_id":navigation.getParam('id'),
        //         "email":navigation.getParam('email'),
        //         "verification_code":navigation.getParam('verification_code'),
        //     };
        //     this.props.recover_new_reset_password(cred);
        // }
        // else{
        //     this.dropdown.alertWithType('error', 'Alert!'," Password is not match !!!!" );
        // }
       
    }

    render() {
        let { width, height } = Dimensions.get('window');
        const underlayColor = "#137271";
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{flex: 1}}>
                    <ImageBackground
                            resizeMode={'cover'} // or cover
                            style={{flex: 1}} // must be passed from the parent, the number may vary depending upon your screen size
                            source={require('../../assets/images/bg.jpg')}
                            >
                    <LinearGradient
                        colors={['rgba(22,88,219,0.5)', 'rgba(64,122,236,0.5)']}
                        style={{flex: 1,
                            flexDirection: 'column'
                            }}>
                            <KeyboardAvoidingView behavior="padding"
                            resetScrollToCoords={{ x: 0, y: 0 }} style={{flex: 1,
                            flexDirection: 'column'
                            }}>
                            <ScrollView  keyboardShouldPersistTaps="handled">
                            <View style={{width: width, height: 0.30*height}}>
                            <Logo/>
                            </View>
                                
                            <View style={{ padding: 20,width: width, flex: 1,flexDirection: 'column'}}>
                                <Text style={styles.normalText}>
                                    Retrieve password.
                                </Text>
                                <Text style={styles.normalText}>
                                Please enter your entered email address.We wiil send you a Code to reset your password.
                                </Text>
                                <Text style={styles.normalText}>
                                    Email Address
                                </Text>
                                <View style={styles.iconView}>
                                  <Icon style={styles.iconIcon} name="ios-lock-outline" />
                                  <TextInput
                                      style={[styles.textInput, styles.iconInput]}
                                      placeholder='Password'
                                      secureTextEntry={true}
                                      placeholderTextColor='rgba(255,255,255,0.5)'
                                      autoCapitalize='none'
                                      underlineColorAndroid='rgba(0,0,0,0)'
                                      onChangeText={(new_password) => this.setState({ new_password })} />
                                </View>
                                <View style={styles.iconView}>
                                  <Icon style={styles.iconIcon} name="ios-lock-outline" />
                                  <TextInput
                                      style={[styles.textInput, styles.iconInput]}
                                      placeholder='Re-Password'
                                      secureTextEntry={true}
                                      placeholderTextColor='rgba(255,255,255,0.5)'
                                      autoCapitalize='none'
                                      underlineColorAndroid='rgba(0,0,0,0)'
                                      onChangeText={(re_password) => this.setState({ re_password })} />
                                </View>
                                
                                <View style={[styles.button, styles.loginButton]}>
                                    <PrimaryButton text='Save' onPress={this.newPasswordSend} />
                                </View>
                               
                                </View>
                                <View>
                                
                            </View>
                            </ScrollView>
                            <DropdownAlert
                                ref={ref => this.dropdown = ref}
                                showCancel={true}
                                updateStatusBar={false}
                                closeInterval={5000}/>
                            </KeyboardAvoidingView>
                    </LinearGradient>
                </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}

PasswordRecovery.propTypes = {
    recover_new_reset_password: PropTypes.func.isRequired,
    
};

const mapStateToProps = state => ({
    recover_new_passwordRes:state.passwordrecover.recover_new_password_res
  });
  
export default connect(mapStateToProps, {
    recover_new_reset_password
  })(PasswordRecovery);
