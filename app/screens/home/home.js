
import React, { Component } from "react";
import PropTypes from 'prop-types';
import {Animated, View, FlatList, Platform, Dimensions,ScrollView} from 'react-native';
import { Tile,Text,Divider} from 'react-native-elements';
import { StatusBarBackground} from '../../components/common/index';
import CommonHeader from '../../components/common/header';
import HeaderSearch from '../../components/common/search';
import {
  connect
} from 'react-redux';
import {
  fetchPackagesData
} from '../../actions/PackagesAction';

import { PublicUrl } from '../../actions/types';

const NAVBAR_HEIGHT = 75;
const {width: SCREEN_WIDTH} = Dimensions.get("window");
const COLOR = "#DADDDF";
class Home extends Component {
  scroll = new Animated.Value(0);
  headerY;

  constructor(props) {
    super(props);
    this.headerY = Animated.multiply(Animated.diffClamp(this.scroll, 0, 75), -1);

    // this.onScroll = this.onScroll.bind(this);
    this.state = {isReady: false,
      offset: 0,
      stickyHeaderIndices: [1],
      data:[]};
    
  }
  componentWillReceiveProps(nextProps){
  }

  componentWillMount(){
      this.props.fetchPackagesData();
  }

  changenavigation(){
console.log("inside");
  }
  
  
  
  render() {
    let { width, height } = Dimensions.get('window');
    const { state, navigate } = this.props.navigation; 
    const tabY = Animated.add(this.scroll, this.headerY);
    return (
      <View style={{flex: 1,
            flexDirection: 'column',backgroundColor:'#DADDDF'}}>
          <StatusBarBackground />
          <CommonHeader />
           
        <Animated.View style={{
          width: "100%",
          position: "absolute",
          transform: [{
            translateY: this.headerY
          }],
          elevation: -1,
          flex: 1,
          zIndex: 0,
          paddingTop:75
        }}>
        
        <HeaderSearch />
        
        </Animated.View>
        <Animated.ScrollView
        scrollEventThrottle={1}
          bounces={false}
          showsVerticalScrollIndicator={false}
          style={{zIndex: 0, height: "100%", elevation: -2}}
          contentContainerStyle={{paddingTop: 55}}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: this.scroll}}}],
            {useNativeDriver: true},
          )}
          overScrollMode="never">
               <Text style={{
                  color:'#033F6C',
                  fontSize:16,
                  marginBottom:8,
                  marginTop:8,
                  paddingLeft:10
                }}>Your Learning Packages</Text>

        <Divider style={{ backgroundColor: '#033F6C' }} />
          <FlatList
          style={{
            flexDirection: 'row'}}
            horizontal
            showsHorizontalScrollIndicator = {false}
            decelerationRate={0.99}
            data={this.props.packages_details}
            keyExtractor={(x,i)=> i.toString()}
            renderItem = {({item})=>
                <Tile
                onPress={() =>this.props.navigation.navigate('Filter',{
                  package_id:item.package_id,
                  Package_name:item.package_name,
                  go_back_key: state.key,
                })}
                activeOpacity={1}
                featured
                imageSrc={{uri: PublicUrl+item.package_image_url}}
                title={item.package_name}
                caption={item.subject_name}
                height={0.3*height}
                width={0.3*width}
                containerStyle={{marginBottom:10,marginTop:10,marginLeft:10,marginRight:10,borderRadius:15}}
                contentContainerStyle={{borderRadius:15}}
                titleStyle={{fontSize:12}}
                imageContainerStyle={{borderRadius:15}}
              />
            }/>
            <Divider style={{ backgroundColor: '#033F6C' }} />
            <Text style={{
                  color:'#033F6C',
                  fontSize:16,
                  marginBottom:8,
                  marginTop:8,
                  paddingLeft:10
                }}>Avaiable Packages</Text>
                  <Divider style={{ backgroundColor: '#033F6C' }} />
            <FlatList
            style={{
              flexDirection: 'column'}}
              numColumns={3}
              showsVerticalScrollIndicator = {true}
              decelerationRate={0.99}
              data={this.props.packages_details}
              keyExtractor={(x,i)=> i.toString()}
              renderItem = {({item})=>
              <Tile
                onPress={() =>this.props.navigation.navigate('Filter',{
                  id:item.package_id,
                  grade_name:item.package_name,
                  go_back_key: state.key,
                })}
                activeOpacity={1}
                featured
                imageSrc={{uri: PublicUrl+item.package_image_url}}
                title={item.package_name}
                caption={item.subject_name}
                height={0.3*height}
                width={0.3*width}
                containerStyle={{marginBottom:10,marginTop:10,marginLeft:10,borderRadius:15}}
                contentContainerStyle={{borderRadius:15}}
                titleStyle={{fontSize:12}}
                imageContainerStyle={{borderRadius:15}}
              />
            }/>
             <Divider style={{ backgroundColor: '#033F6C' }} />
            <Text style={{
                  color:'#033F6C',
                  fontSize:16,
                  marginBottom:8,
                  marginTop:8,
                  paddingLeft:10
                }}>Top Packages</Text>
                  <Divider style={{ backgroundColor: '#033F6C' }} />
            <FlatList
            style={{
              flexDirection: 'column'}}
              numColumns={3}
              showsVerticalScrollIndicator = {true}
              decelerationRate={0.99}
              data={this.props.packages_details}
              keyExtractor={(x,i)=> i.toString()}
              renderItem = {({item})=>
              <Tile
                onPress={() =>this.props.navigation.navigate('Filter',{
                  id:item.package_id,
                  grade_name:item.package_name,
                  go_back_key: state.key,
                })}
                activeOpacity={1}
                featured
                imageSrc={{uri: PublicUrl+item.package_image_url}}
                title={item.package_name}
                caption={item.subject_name}
                height={0.3*height}
                width={0.3*width}
                containerStyle={{marginBottom:10,marginTop:10,marginLeft:10,borderRadius:15}}
                contentContainerStyle={{borderRadius:15}}
                titleStyle={{fontSize:12}}
                imageContainerStyle={{borderRadius:15}}
              />
            }/>
        </Animated.ScrollView>
      </View>
    );
  }

}

Home.propTypes = {
  fetchPackagesData: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
  packages_details: state.packagesdata.packages,
});

export default connect(mapStateToProps, {
  fetchPackagesData
})(Home);