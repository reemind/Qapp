import React, { Component } from 'react';
import styles from '../../assets/styles';
import PropTypes from 'prop-types';

import DropdownAlert from 'react-native-dropdownalert';
import validateEmail from '../../helpers/validator';
import Icon from 'react-native-vector-icons/Ionicons';
import { LinearGradient } from 'expo';
import { Text, View, Platform, Dimensions, StatusBar,ImageBackground, TextInput,TouchableWithoutFeedback,
    KeyboardAvoidingView, Keyboard,TouchableHighlight, ScrollView, ModalSelector,Modal} from 'react-native';
import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
import messages from '../../data/messages';
import { connect } from 'react-redux';
import { new_user_register } from '../../actions/LoginSignUpAction';
import { recoveryEmailVerification } from '../../actions/PasswordRecoveryAction';


 class Register extends Component {
    constructor(props) {
        super(props);
        this.signUpPress = this.signUpPress.bind(this);
        this.codeverification = this.codeverification.bind(this);
        this.goBack = this.goBack.bind(this);
        this.state = {
            verification_code:'',
            full_name: '',
            email: '',
            mobile_no: '',
            password: '',
            modalVisible: false,
            id:''
        };
    }

    componentWillReceiveProps(nextProps){

        if(typeof nextProps.RecoverEmailVerificationRes.success !== "undefined"){
            if(nextProps.RecoverEmailVerificationRes.success){
                this.setState({modalVisible: false});
                this.props.navigation.navigate('InitialScreen');
            }
            else{
                this.dropdown.alertWithType('error', 'Alert!', 'nextProps.verificationRes.error');
            } 
        }
        else if (nextProps.NewUserRegisterRes) {
            if(nextProps.NewUserRegisterRes.success){
                this.setState({
                    email:nextProps.NewUserRegisterRes.email,
                    id:nextProps.NewUserRegisterRes.id
                })
                this.setState({modalVisible: true});
            }
            else{
                this.dropdown.alertWithType('error', 'Alert!',"nextProps.RecoverPostEmail.error" );
            }
        }
    }

    codeverification(e){
        e.preventDefault();
        this.setState({modalVisible: false});
        this.props.navigation.navigate('InitialScreen');
    //     const veri = {
    //         "verification_code": this.state.verification_code,
    //         "email":this.state.email,
    //         "id":this.state.id
    //     };
    //    if(this.state.verification_code != ''){
    //         this.props.recoveryEmailVerification(veri)
    //    }
    //    else{
    //     this.dropdown.alertWithType('error', 'Alert!', 'Please enter your Code');
    //    }

    }

    signUpPress(e) {
        e.preventDefault();
        this.setState({modalVisible: true});
        // const userdetails = {
        //     "email":this.state.email,
        //     "full_name":this.state.full_name,
        //     "mobile_no": this.state.mobile_no,
        //     "password": this.state.password
        // }
        // if (this.state.email != '') {
        //     if (this.state.password != '') {
        //         this.props.new_user_register(userdetails);
        //     }
        //     else {
        //         this.dropdown.alertWithType('error', 'Alert!', 'Please enter your password');
        //     }
        // }
        // else {
        //     this.dropdown.alertWithType('error', 'Alert!', 'Please enter your email');
        // }
    }

    goBack() {
      this.props.navigation.navigate('InitialScreen');
    }

    render() {
        let { width, height } = Dimensions.get('window');
        const underlayColor = "#137271";
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{flex: 1}}>
                <ImageBackground
                    resizeMode={'cover'} // or cover
                    style={{flex: 1}} // must be passed from the parent, the number may vary depending upon your screen size
                    source={require('../../assets/images/bg.jpg'
                    )}>
                    <LinearGradient
                        colors={['rgba(22,88,219,0.5)', 'rgba(64,122,236,0.5)']}
                        style={{flex: 1,
                        flexDirection: 'column'
                    }}>
                    <KeyboardAvoidingView behavior="padding"
                    resetScrollToCoords={{ x: 0, y: 0 }} style={{flex: 1,
                    flexDirection: 'column'
                    }}>
                    <ScrollView  keyboardShouldPersistTaps="handled">
                        <View style={{width: width, height: 0.30*height}}>
                        <Logo/>
                        </View>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                alert('Modal has been closed.');
                            }}>
                            <View style={{ width:0.92*width,height:0.55*height, backgroundColor:"#aaa",
                                marginLeft:0.04*width, marginTop:0.20*height,flexDirection: 'column'}}>
                                <Text style={styles.normalText}>
                                    Verification Code.
                                </Text>
                                <Text style={styles.normalText}>
                                    Please enter your Code.
                                </Text>
                            
                                <View style={styles.iconView} >
                                    <Icon style={styles.iconIcon} name="ios-mail-outline" />
                                    <TextInput
                                        style={[styles.textInput, styles.iconInput]}
                                        placeholder='Verification Code'
                                        placeholderTextColor='rgba(255,255,255,0.5)'
                                        keyboardType='numeric'
                                        autoCapitalize='none'
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        onChangeText={(verification_code) => this.setState({ verification_code })
                                        }/>
                                </View>
                                <View style={[styles.button, styles.loginButton]}>
                                    <PrimaryButton text='Send' onPress={this.codeverification} />
                                </View>
                            </View>
                        </Modal>
                    <View style={{ padding: 20,width: width, flex: 1,flexDirection: 'column'}}>
                        <View style={styles.iconView}>
                          <Icon style={styles.iconIcon} name="ios-lock-outline" />
                          <TextInput
                              style={[styles.textInput, styles.iconInput]}
                              placeholder='Full Name *'
                              placeholderTextColor='rgba(255,255,255,0.5)'
                              autoCapitalize='none'
                              underlineColorAndroid='rgba(0,0,0,0)'
                              onChangeText={(full_name) => this.setState({ full_name })} />
                        </View>
                        <View style={styles.iconView}>
                          <Icon style={styles.iconIcon} name="ios-mail-outline" />
                          <TextInput
                            style={[styles.textInput, styles.iconInput]}
                            placeholder='Email Address *'
                            placeholderTextColor='rgba(255,255,255,0.5)'
                            keyboardType='email-address'
                            autoCapitalize='none'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(email) => this.setState({ email })}
                          />
                        </View>

                        <View style={styles.iconView}>
                          <Icon style={styles.iconIcon} name="ios-call-outline" />
                          <TextInput
                            style={[styles.textInput, styles.iconInput]}
                            placeholder='Mobile Number *'
                            placeholderTextColor='rgba(255,255,255,0.5)'
                            keyboardType='phone-pad'
                            autoCapitalize='none'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(mobile_no) => this.setState({ mobile_no })}
                          />
                        </View>
                        <View style={styles.iconView}>
                          <Icon style={styles.iconIcon} name="ios-lock-outline" />
                          <TextInput
                            style={[styles.textInput, styles.iconInput]}
                            placeholder='Password *'
                            placeholderTextColor='rgba(255,255,255,0.5)'
                            keyboardType='default'
                            secureTextEntry={true}
                            autoCapitalize='none'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            onChangeText={(password) => this.setState({ password })}
                          />
                        </View>
                        <View style={[styles.button, styles.loginButton]}>
                            <PrimaryButton text='SIGN UP'
                                onPress={this.signUpPress}
                            />
                        </View>
                        <View >
                            <TouchableHighlight onPress={this.goBack} >
                                <View style={styles.bottomMessage}>
                                  <Icon style={[styles.iconNormal, styles.bigIcon]} name="ios-arrow-round-back" />
                                  <Text style={[styles.textWhite, styles.mlSm]}>
                                      Back To Login
                                   </Text>
                                 </View>
                            </TouchableHighlight>
                        </View>

                        <DropdownAlert
                            ref={ref => this.dropdown = ref}
                            showCancel={true}
                            updateStatusBar={false}
                            closeInterval={5000} />
                    </View>
                    </ScrollView>
                    </KeyboardAvoidingView>
            </LinearGradient>
            </ImageBackground>
            </TouchableWithoutFeedback>
        );
    }
}

Register.propTypes = {
    new_user_register: PropTypes.func.isRequired ,
       recoveryEmailVerification:PropTypes.func.isRequired

  };

const mapStateToProps = state => ({
    NewUserRegisterRes: state.loginsignupuser.newusersignup,
    RecoverEmailVerificationRes:state.passwordrecover.recovery_email_verification_res

  });

export default connect(mapStateToProps,{
    new_user_register,
    recoveryEmailVerification
})(Register);