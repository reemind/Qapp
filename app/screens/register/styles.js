import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

let { width, height } = Dimensions.get('window');
export default EStyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: height,
        padding: 20
    },
    bottomText: {
        color: 'black',
        textDecorationLine: 'underline',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginView: {

    },
    signUpView: {
    },
    textInput: {
        color: 'white',
        borderRadius: 25,
        height: 50,
        paddingHorizontal: 20,
        marginTop: 5,
        marginBottom: 5,
        width: width-40,
        fontSize: 14,
        backgroundColor:'rgba(255,255,255,0.5)'
    },
    rememberText: {
        fontSize: 15,
        color: 'white',
        marginTop:5,
        marginBottom:5,
        textAlign: 'right'
    },
    centerText: {
      marginLeft: 'auto',
      marginRight: 'auto',
      color: 'white',
      fontSize: 15,
      marginTop: 15
    },
    loginButton: {
      marginTop: 20,
      height: 50
    },
    iconView: {
      display: 'flex',
      justifyContent: 'center'
    },
    iconIcon: {
      paddingLeft: 15,
      position: 'absolute',
      fontSize: 20,
      color: 'rgba(255,255,255,0.9)'
    },
    iconInput: {
      paddingLeft: 50
    },
    forgotContainer: {

    }
});
