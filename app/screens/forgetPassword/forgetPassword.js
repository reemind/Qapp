import React, { Component } from 'react';
import styles from './styles';
import PropTypes from 'prop-types';

import DropdownAlert from 'react-native-dropdownalert';
import validateEmail from '../../helpers/validator';
import { LinearGradient } from 'expo';
import Icon from 'react-native-vector-icons/Ionicons';

import { Text, View, Platform, Dimensions,StatusBar,ImageBackground, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView,
    Keyboard, TouchableHighlight,ScrollView,ModalSelector, Modal} from 'react-native';

import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
// import messages from '../../data/messages';
import { connect } from 'react-redux';
import { postRecoveryEmail } from '../../actions/PasswordRecoveryAction';

import { recoveryEmailVerification } from '../../actions/PasswordRecoveryAction';


class ForgetPassword extends Component {

    constructor(props) {
        super(props);
        this.forgotpassword = this.forgotpassword.bind(this);
        this.codeverification = this.codeverification.bind(this);
       
        this.state = {
            email: '',
            verification_code:'',
            id:'',
            modalVisible: false,
        };
       
    }
    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        if(typeof nextProps.RecoverEmailVerificationRes.success !== "undefined"){
            if(nextProps.RecoverEmailVerificationRes.success){
                this.setState({modalVisible: false});
                this.props.navigation.navigate('PasswordRecovery',{
                    id:nextProps.RecoverEmailVerificationRes.id,
                    email:nextProps.RecoverEmailVerificationRes.email,
                    verification_code:nextProps.RecoverEmailVerificationRes.verification_code
                });
                
                
            }
           else{
            this.dropdown.alertWithType('error', 'Alert!', 'nextProps.verificationRes.error');
           } 
        }
        else if (nextProps.RecoverPostEmail) {
            console.log('here');
            if(nextProps.RecoverPostEmail.success){
                this.setState({
                    email:nextProps.RecoverPostEmail.email,
                    id:nextProps.RecoverPostEmail.id
                })
                this.setState({modalVisible: true});
            }
            else{
                this.dropdown.alertWithType('error', 'Alert!',nextProps.RecoverPostEmail.error );
            }
        }
      }

    forgotpassword(e) {
        e.preventDefault();
        this.setState({modalVisible: true});
        // const cred = {
        //     "email": this.state.email,
        // };

        // if (this.state.email != '') {
        //       this.props.postRecoveryEmail(cred);
        // }
        // else {
        //     this.dropdown.alertWithType('error', 'Alert!', 'Please enter your email');
        // }
    }

    codeverification(e){

        e.preventDefault();
        this.setState({modalVisible: false});
        this.props.navigation.navigate('PasswordRecovery');
    //     const veri = {
    //         "verification_code": this.state.verification_code,
    //         "email":this.state.email,
    //         "id":this.state.id
    //     };
    //    if(this.state.verification_code != ''){
    //         this.props.recoveryEmailVerification(veri)
    //    }
    //    else{
    //     this.dropdown.alertWithType('error', 'Alert!', 'Please enter your Code');
    //    }

    }

    render() {
        let { width, height } = Dimensions.get('window');
        const underlayColor = "#137271";
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{flex: 1}}>
                    <ImageBackground
                            resizeMode={'cover'} // or cover
                            style={{flex: 1}} // must be passed from the parent, the number may vary depending upon your screen size
                            source={require('../../assets/images/bg.jpg')}
                            >
                    <LinearGradient
                        colors={['rgba(22,88,219,0.5)', 'rgba(64,122,236,0.5)']}
                        style={{flex: 1,
                            flexDirection: 'column'
                            }}>
                            <KeyboardAvoidingView behavior="padding"
                            resetScrollToCoords={{ x: 0, y: 0 }} style={{flex: 1,
                            flexDirection: 'column'
                            }}>
                            <ScrollView  keyboardShouldPersistTaps="handled">
                            <View style={{width: width, height: 0.30*height}}>
                            <Logo/>
                            </View>
                                <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.modalVisible}
                                onRequestClose={() => {
                                    alert('Modal has been closed.');
                                }}>
                                <View style={{ width:0.92*width,height:0.55*height, backgroundColor:"#aaa",
                                marginLeft:0.04*width, marginTop:0.20*height,flexDirection: 'column'}}>
                                     <Text style={styles.normalText}>
                                        Verification Code.
                                    </Text>
                                    <Text style={styles.normalText}>
                                    Please enter your Code.
                                    </Text>
                                    
                                    <View style={styles.iconView} >
                                        <Icon style={styles.iconIcon} name="ios-mail-outline" />
                                        <TextInput
                                            style={[styles.textInput, styles.iconInput]}
                                            placeholder='Verification Code'
                                            placeholderTextColor='rgba(255,255,255,0.5)'
                                            keyboardType='numeric'
                                            autoCapitalize='none'
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            onChangeText={(verification_code) => this.setState({ verification_code })} />
                                    </View>
                                    <View style={[styles.button, styles.loginButton]}>
                                        <PrimaryButton text='Send' onPress={this.codeverification} />
                                    </View>
                                </View>
                                </Modal>
                            <View style={{ padding: 20,width: width, flex: 1,flexDirection: 'column'}}>
                                <Text style={styles.normalText}>
                                    Retrieve password.
                                </Text>
                                <Text style={styles.normalText}>
                                Please enter your entered email address.We wiil send you a Code to reset your password.
                                </Text>
                                <Text style={styles.normalText}>
                                    Email Address
                                </Text>
                                <View style={styles.iconView} >
                                  <Icon style={styles.iconIcon} name="ios-mail-outline" />
                                  <TextInput
                                      style={[styles.textInput, styles.iconInput]}
                                      placeholder='Email'
                                      placeholderTextColor='rgba(255,255,255,0.5)'
                                      keyboardType='email-address'
                                      autoCapitalize='none'
                                      underlineColorAndroid='rgba(0,0,0,0)'
                                      onChangeText={(email) => this.setState({ email })} />
                                </View>
                                
                                
                                <View style={[styles.button, styles.loginButton]}>
                                    <PrimaryButton text='Send' onPress={this.forgotpassword} />
                                </View>
                               
                                </View>
                                <View>
                                
                            </View>
                            </ScrollView>
                            <DropdownAlert
                                ref={ref => this.dropdown = ref}
                                showCancel={true}
                                updateStatusBar={false}
                                closeInterval={5000}/>
                            </KeyboardAvoidingView>
                    </LinearGradient>
                    </ImageBackground>
                    </TouchableWithoutFeedback>
        );
    }
}
ForgetPassword.propTypes = {
    postRecoveryEmail: PropTypes.func.isRequired,
    recoveryEmailVerification:PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    RecoverPostEmail: state.passwordrecover.post_recover_mail,
    RecoverEmailVerificationRes:state.passwordrecover.recovery_email_verification_res
  });
  
export default connect(mapStateToProps, {
    postRecoveryEmail,
    recoveryEmailVerification
  })(ForgetPassword);
