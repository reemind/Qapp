import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

let {width, height} = Dimensions.get('window');

export default EStyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '$primaryDarkBlue',
        height: height,
        padding: 10,
    },
    textButton:{
        color: '$primaryBlue',
        fontSize: 15
    },
    normalText:{
        fontSize: 15,
        marginTop: 5,
        padding:5
    },
    textInput: {
        alignItems: 'center',
        color: 'white',
        borderRadius: 25,
        height: 50,
        paddingHorizontal: 20,
        marginTop: 5,
        marginBottom: 5,
        width: width-40,
        fontSize: 14,
        backgroundColor:'rgba(255,255,255,0.5)'
    },
    termsContainer:{
        flexDirection: 'row',
        width: width-40,
        marginLeft: 20,
        marginTop: 20,
    },
    termsButton:{
        height: 25,
        width: 25,
        marginRight: 10,
        borderColor: '$primaryBlue',
        borderWidth: 1,
        borderRadius: 2,
        color: 'white',
        textAlign: 'center',
        fontSize: 20
    },
    
    iconView: {
        display: 'flex',
        justifyContent: 'center'
      },
      iconIcon: {
        paddingLeft: 15,
        position: 'absolute',
        fontSize: 20,
        color: 'rgba(255,255,255,0.9)'
      },
      iconInput: {
        paddingLeft: 50
      },
      loginButton: {
        marginTop: 20,
        height: 50
      },
      modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#f7021a',
        padding: 100
     },
});