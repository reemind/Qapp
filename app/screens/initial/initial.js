import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from '../../assets/styles';
import DropdownAlert from 'react-native-dropdownalert';
import validateEmail from '../../helpers/validator';
import Icon from 'react-native-vector-icons/Ionicons';
import { LinearGradient } from 'expo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
<script src="http://172.18.24.162:8097"></script>
import {
    Text, View, Platform, Dimensions, StatusBar,
    ImageBackground, TextInput, TouchableWithoutFeedback, KeyboardAvoidingView,
    Keyboard, TouchableHighlight, AsyncStorage, ScrollView
} from 'react-native';

import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
import messages from '../../data/messages';

import { connect } from 'react-redux';
import { usersignup } from '../../actions/LoginSignUpAction';
import {token} from '../../actions//types';
import * as types from '../../actions/types';


class Initial extends Component {
    constructor(props) {
        super(props);
        this.loginPress = this.loginPress.bind(this);
        this.forgetPasswordPress = this.forgetPasswordPress.bind(this);
        this.signUpPress = this.signUpPress.bind(this);
        this.state = {
            email: 'raj@test.com',
            Password: '1234',
            isReady: false
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userCred) {
            if(nextProps.userCred.token){
                AsyncStorage.setItem('tokenkey', nextProps.userCred.token).then(this.props.navigation.navigate('Sidemenu'));
                types.token = nextProps.userCred.token;
            }
       else{
        // console.log(nextProps.userCred);
                this.dropdown.alertWithType('error', 'Alert!', nextProps.userCred.error);
                this.props.navigation.navigate('RootStack')
            }
        }
      }
    loginPress(e) {
        e.preventDefault();
        this.props.navigation.navigate('Sidemenu');

        // const cred = {
        //     "email": this.state.email,
        //     "password": this.state.Password
        // };
    
        // if (this.state.email != '') {
        //     if (this.state.Password != '') {
        //         this.props.usersignup(cred);
        //     }
        //     else {
        //         this.dropdown.alertWithType('error', 'Alert!', 'Please enter your password');
        //     }
        // }
        // else {
        //     this.dropdown.alertWithType('error', 'Alert!', 'Please enter your email');
        // }
    }
    signUpPress() {
        this.props.navigation.navigate('Register');
    }
    forgetPasswordPress() {
        this.props.navigation.navigate('ForgetPassword');
    }
    render() {
        let { width, height } = Dimensions.get('window');
        const underlayColor = "#137271";
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} style={{flex: 1}}>
                    <ImageBackground
                            resizeMode={'cover'} // or cover
                            style={{flex: 1}} // must be passed from the parent, the number may vary depending upon your screen size
                            source={require('../../assets/images/bg-screen.jpg')}
                            >
                    <LinearGradient
                        colors={['rgba(22,88,219,0.5)', 'rgba(64,122,236,0.5)']}
                        style={{flex: 1,
                            flexDirection: 'column'
                            }}>
                            <KeyboardAvoidingView behavior="padding"
                            resetScrollToCoords={{ x: 0, y: 0 }} style={{flex: 1,
                            flexDirection: 'column'
                            }}>
                            <ScrollView  keyboardShouldPersistTaps="handled">
                            <View style={{width: width, height: 0.30*height}}>
                            <Logo/>
                            </View>
                            <View style={{ padding: 20,width: width, flex: 1,flexDirection: 'column'}}>
                                <View style={styles.iconView} >
                                  <Icon style={styles.iconIcon} name="ios-mail-outline" />
                                  <TextInput
                                      style={[styles.textInput, styles.iconInput]}
                                      placeholder='Email'
                                      placeholderTextColor='rgba(255,255,255,0.5)'
                                      keyboardType='email-address'
                                      autoCapitalize='none'
                                      underlineColorAndroid='rgba(0,0,0,0)'
                                      onChangeText={(email) => this.setState({ email })}
                                      value={this.state.email} />
                                </View>
                                <View style={styles.iconView}>
                                  <Icon style={styles.iconIcon} name="ios-lock-outline" />
                                  <TextInput
                                      style={[styles.textInput, styles.iconInput]}
                                      placeholder='Password'
                                      secureTextEntry={true}
                                      placeholderTextColor='rgba(255,255,255,0.5)'
                                      autoCapitalize='none'
                                      underlineColorAndroid='rgba(0,0,0,0)'
                                      onChangeText={(Password) => this.setState({ Password })} 
                                      value={this.state.Password}
                                      />
                                </View>
                                <View style={styles.forgotContainer}>
                                    <TouchableHighlight onPress = {this.forgetPasswordPress} underlayColor='#017171'>
                                        <Text style={styles.rememberText}>
                                            Forgot Password?
                                         </Text>
                                    </TouchableHighlight>
                                </View>
                                <View style={[styles.button, styles.loginButton]}>
                                    <PrimaryButton text='Login' onPress={this.loginPress} />
                                </View>
                                <View style={{
                                        alignItems: 'center',
                                        marginBottom: 30
                                    }}>
                                    <View style={styles.bottomMessage}>
                                        <Text
                                            style={styles.signUpText}>
                                            Don't have an account?
                                        </Text>
                                        <TouchableHighlight onPress={this.signUpPress}>
                                            <Text
                                                style={[styles.signUpText, styles.mlSm]}>
                                                Sign Up
                                            </Text>
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                <View>
                                <View style={{ flex: 1,flexDirection: 'row'}}>
                                <TouchableHighlight>
                                <Text
                                    style={[styles.signUpText, styles.mlSm]}>
                                    <Icon style={styles.iconSocial} name="logo-facebook" />
                                </Text>
                                </TouchableHighlight>
                                <TouchableHighlight>
                                <Text
                                    style={[styles.signUpText, styles.mlSm]}>
                                     <Icon style={styles.iconSocial} name="logo-google" />
                                </Text>
                                </TouchableHighlight>
                            </View>
                            </View>
                            </View>
                            </ScrollView>
                            <DropdownAlert
                                ref={ref => this.dropdown = ref}
                                showCancel={true}
                                updateStatusBar={false}
                                closeInterval={5000}/>

                            </KeyboardAvoidingView>
                    </LinearGradient>
                    </ImageBackground>
                    </TouchableWithoutFeedback>

        );
    }
}
Initial.propTypes = {
    usersignup: PropTypes.func.isRequired
  };
const mapStateToProps = state => ({
    userCred: state.loginsignupuser.usercredential,

  });
  
  export default connect(mapStateToProps, {
    usersignup
  })(Initial);