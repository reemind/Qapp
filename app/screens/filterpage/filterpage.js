
import React, { Component } from "react";
import { FlatList, Dimensions,View, ScrollView} from 'react-native';
import {List, ListItem, Text} from 'react-native-elements';
import {Button} from "native-base";
import {
  connect
} from 'react-redux';
import {
  fetchPackagesDetails
} from '../../actions/PackagesAction';
import PropTypes from 'prop-types';
import { PublicUrl } from '../../actions/types';
import CommonHeader from '../../components/common/header';
import HeaderSearch from '../../components/common/search';
import { StatusBarBackground} from '../../components/common/index';

class Filterpage extends Component {
  constructor(props) {
    super(props);
    this.state = {isReady: false};
  }
  componentWillReceiveProps(nextProps){
  }
  componentWillMount(){
    const { navigation } = this.props;
    const PackageId = navigation.getParam('package_id', 'NO-ID');
    this.props.fetchPackagesDetails(PackageId);
}

  render() {
    let { width, height } = Dimensions.get('window');
    const { navigation } = this.props;
    const { state, goBack } = this.props.navigation; 
    const go_back_key = navigation.getParam('go_back_key');
    const PackageId = navigation.getParam('package_id', 'NO-ID');
    const Package_name = navigation.getParam('Package_name', 'no data found');
    return (
      <View style={{flex: 1,
        flexDirection: 'column'}}>
        <StatusBarBackground />
        <ScrollView
        stickyHeaderIndices={[1]}
        contentOffset={{x:10,y:10}}
        style={{
        flex:1,
        flexDirection: 'column',
        backgroundColor:'#DADDDF'}}>
        <CommonHeader />
        <HeaderSearch />
        <Text style={{
                  color:'#033F6C',
                  fontSize:16,
                  marginBottom:8,
                  marginTop:8,
                  paddingLeft:10
                }}>{Package_name}</Text>
          <List containerStyle={{marginBottom: 20}}>
          {
            this.props.packages_detailwithchapter.map((l) => (
              <ListItem
                roundAvatar
                avatar={{uri: PublicUrl+l.chapter_image_url}}
                key={l.chapter_id}
                title={l.chapter_name}
              />
            ))
          }
        </List>
        

        <Button 
          onPress={() => {this.props.navigation.navigate(go_back_key);}}
          full rounded primary
            style={{ marginTop: 10 }}
            >
            <Text>Goto Back</Text>
          </Button>
        </ScrollView>
      </View>
    );
  }

}


Filterpage.propTypes = {
  fetchPackagesDetails: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
  packages_detailwithchapter: state.packagesdata.packagedetails,
});

export default connect(mapStateToProps, {
  fetchPackagesDetails
})(Filterpage);