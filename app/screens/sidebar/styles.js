import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

let {width, height} = Dimensions.get('window');

export default EStyleSheet.create({
    listitem: {
        alignItems: 'center',
        flexDirection:'row',
        paddingRight:0,
        paddingTop:12,
        paddingBottom:12,
        marginLeft:0,
        borderBottomWidth:0.5,
        borderTopWidth:0.5,
        borderColor:'#c9c9c9',
    },
    listtext:{
        color:'#033F6C',
        fontSize:16
    },
    listIcon:{
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
        fontSize:16,
        color:'#033F6C'
    },
    listlable:{
        flexDirection:'row',
        marginTop:10,
        marginBottom:10,
        marginLeft:20,
        marginRight:5
    },
    textinput:{
        alignItems: 'center',
        width: 0.65*width,
        fontSize: 14,
        paddingLeft:10,
        marginRight:10,
        backgroundColor:'#DADDDF'
    }

    
});