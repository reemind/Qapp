import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import { ImageBackground,View, ScrollView, StyleSheet, Button, StatusBar,AppRegistry, Image } from 'react-native';
import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
import { Container, Content, Text, List, ListItem,Icon } from "native-base";
import { LinearGradient } from 'expo';
import styles from './styles';

const routes = ["Home", "Setting"];

class SideBar extends Component {

  navigateToScreen = (route) => () => {
    const navigate = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigate);
  }
  

  render () {
    return (
      <Container>
        <Content style={{backgroundColor:'#DADDDF'}}>
          <ImageBackground
            resizeMode={'cover'} // or cover
            style={{height: 180}} // must be passed from the parent, the number may vary depending upon your screen size
            source={require('../../assets/images/bg.jpg')}>
            <LinearGradient
                        colors={['rgba(22,88,219,0.5)', 'rgba(64,122,236,0.5)']}
                        style={{height: 180,
                            flexDirection: 'column',
                            alignItems:'center',
                            }}>
            <Image 
            source={require('../../assets/images/main.jpg')}
            style={{marginTop:0.20*180,height: 100,width:100, borderRadius: 50}}
             />
             <Text>Raj Kumar Khanal</Text>
            </LinearGradient>
          </ImageBackground>
          <List>
          <ListItem style={styles.listitem} 
           button
                onPress={() => this.props.navigation.navigate('Home')}>
                <Icon  name="home"
                style={styles.listIcon}/>
                <Text style={styles.listtext}>Dashboard</Text>
            </ListItem>

            <ListItem style={styles.listitem} 
           button
                onPress={() => this.props.navigation.navigate('Profile')}>
                <Icon  name="person"
                style={styles.listIcon}/>
                <Text style={styles.listtext}>Profile</Text>
            </ListItem>
            <ListItem style={styles.listitem} 
           button
                onPress={() => this.props.navigation.navigate('Setting')}>
                <Icon  name="settings"
                style={styles.listIcon}/>
                <Text style={styles.listtext}>Setting</Text>
            </ListItem>

            <ListItem style={styles.listitem} 
           button
                onPress={() => this.props.navigation.navigate('QuestionTab')}>
                <Icon  name="settings"
                style={styles.listIcon}/>
                <Text style={styles.listtext}>Question Tab View</Text>
            </ListItem>
         </List>
        </Content>
      </Container>
    );
  }
}


export default SideBar;