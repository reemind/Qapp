
import React, { Component } from "react";
import { View, ScrollView, StyleSheet, StatusBar } from 'react-native';
import { PrimaryButton, StatusBarBackground, Logo } from '../../components/common/index';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content,Text, Card, CardItem } from "native-base";
class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {isReady: false};
  }

  static navigationOptions = {
    drawerLabel: 'Settings',
    drawerIcon: ({ tintColor }) => (
      <Icon name='settings' />
    ),
  };
 
  render() {
    return (
      <Container>
      <StatusBarBackground></StatusBarBackground>
        <Header>
          <Left>
          <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" />
        </Button>
          </Left>
          <Body>
            <Title>Seeting Pages</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>Seeting pages and to user App to talk some awesome people!</Text>
              </Body>
            </CardItem>
          </Card>
          <Button full rounded dark
            style={{ marginTop: 10 }}
           >
            <Text>Chat With setting pages</Text>
          </Button>
          <Button full rounded primary
            style={{ marginTop: 10 }}
            >
            <Text>Goto Setting</Text>
          </Button>
        </Content>
      </Container>
    );
  }

}


export default Setting;