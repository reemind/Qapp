import * as React from 'react';
import {Animated, View,Text,StyleSheet, Dimensions } from 'react-native';
import {
  TabView,
  TabBar,
  SceneMap,
  Route,
  NavigationState,
} from 'react-native-tab-view';
import { Constants } from 'expo';
import { StatusBarBackground} from '../../components/common/index';
import CommonHeader from '../../components/common/header';
import Registerpage from '../../screens/register/register'

const FirstRoute = () => (
  <Animated.ScrollView
  scrollEventThrottle={1}
    bounces={false}
    showsVerticalScrollIndicator={false}
    style={{zIndex: 0, height: "100%", elevation:1}}
    contentContainerStyle={{paddingTop: 55}}
    onScroll={Animated.event(
      [{nativeEvent: {contentOffset: {y: this.scroll}}}],
      {useNativeDriver: true},
    )}
    overScrollMode="never">
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    <Text>I'm trying to create a circle with a white background in react native and i'm having an issue where the background color of the circle is seen on the outline of the border.</Text>
    </Animated.ScrollView>
);
const SecondRoute = () => (
  <View style={[styles.container, { backgroundColor: '#673ab7' }]} />
);
const ThreeRoute = () => (
  <View style={[styles.container, { backgroundColor: '#ff0000' }]} />
);
const fourRoute = () => (
  <View style={[styles.container, { backgroundColor: '#4d4dff' }]} />
);
const fiveRoute = () => (
  <View style={[styles.container, { backgroundColor: '#33ffad' }]} />
);
const sixRoute = () => (
  <View style={[styles.container, { backgroundColor: '#ff751a' }]} />
);
const sevenRoute = () => (
  <View style={[styles.container, { backgroundColor: '#bf4080' }]} />
);
const eightRoute = () => (
  <View style={[styles.container, { backgroundColor: '#cc0000' }]} />
);
const nineRoute = () => (
  <View style={[styles.container, { backgroundColor: '#33cc33' }]} />
);

// type State = NavigationState<
//   Route<{
//     key: string,
//     title: string,
//   }>
// >;

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

export default class TabViewExample extends React.Component {
  scroll = new Animated.Value(0);
  headerY;
  state = {
    index: 0,
    routes: [
      { key: 'first', title: '1' },
      { key: 'second', title: '21' },
      { key: 'three', title: '55' },
      { key: 'four', title: '88' },
      { key: 'five', title: '3' },
      { key: 'six', title: '9' },
      { key: 'seven', title: '5' },
      { key: 'eight', title: '7' },
      { key: 'nine', title: '9' },
      
      
    ],
  };
  constructor(props) {
    super(props);
    this.headerY = Animated.multiply(Animated.diffClamp(this.scroll, 0, 40), -1);
    
  }

 _handleIndexChange = index =>
    this.setState({
      index,
    });

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
     
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
    />
  );


  _renderScene = SceneMap({
    first: Registerpage,
    second: SecondRoute,
    three: ThreeRoute,
    four: fourRoute,
    five: fiveRoute,
    six: sixRoute,
    seven: sevenRoute,
    eight: eightRoute,
    nine: nineRoute,
  });

  render() {
    return (
      <View style={{flex: 1,
        flexDirection: 'column',backgroundColor:'#DADDDF'}}>
        <StatusBarBackground />
        <Animated.View style={{
          width: "100%",
          position: "absolute",
          transform: [{
            translateY: this.headerY
          }],
          elevation:1,
          flex: 1,
          zIndex:2,
          paddingTop:24
        }}>
        
        <CommonHeader />
        
        </Animated.View>
          
        <TabView
          style={[styles.container, this.props.style]}
          navigationState={this.state}
          renderScene={this._renderScene}
          renderTabBar={this._renderTabBar}
          onIndexChange={this._handleIndexChange}
          initialLayout={initialLayout}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex:1,
    paddingTop:50
  },
  tabbar: {
    backgroundColor: '#DADDDF',
  },
  tab: {
    width: 50,
    borderRadius: 23,
    width: 50,
    height: 46,
    borderColor: '#ffeb3a',
    borderWidth: 1,
    margin:5
    
  },
 
  label: {
    color: '#000',
    fontWeight: '400',
    
  },
});
