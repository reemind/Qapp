import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

let { width, height } = Dimensions.get('window');
export default EStyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        height: height,
        padding: 20
    },
    bottomText: {
        color: 'black',
        textDecorationLine: 'underline'
    },
    textWhite: {
      color: '#ffffff'
    },
    loginView: {

    },
    signUpView: {
    },
    textInput: {
        color: 'white',
        borderRadius: 25,
        height: 50,
        paddingHorizontal: 20,
        marginTop: 5,
        marginBottom: 5,
        width: width-40,
        fontSize: 14,
        backgroundColor:'rgba(255,255,255,0.5)'
    },
    rememberText: {
        fontSize: 15,
        color: 'white',
        marginTop:5,
        marginBottom:5,
        textAlign: 'right'
    },
    signUpText: {
      color: 'white',
      fontSize: 15
    },
    loginButton: {
      marginTop: 30,
      marginBottom: 30,
      height: 55
    },
    iconView: {
      display: 'flex',
      justifyContent: 'center'
    },
    iconIcon: {
      paddingLeft: 15,
      position: 'absolute',
      fontSize: 20,
      color: 'rgba(255,255,255,0.9)'
    },
    iconInput: {
      paddingLeft: 50
    },
    forgotContainer: {

    },
    bottomMessage: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
    },
    iconNormal: {
      fontSize:20,
      color: 'rgba(255,255,255,0.9)'
    },
    socialWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 25,
      height: 60,
      paddingHorizontal: 20,
      marginBottom: 10
    },
    socialFacebook: {
      backgroundColor: '#3B5998'
    },
    socialGoogle: {
      backgroundColor: '#db3236'
    },
    iconSocial: {
      fontSize: 32,
      color: '#ffffff',
      marginLeft: 15,
      marginRight: 15
    },
    socialText: {
      color: '#ffffff',
      fontSize: 14
    },
    mlSm: {
      marginLeft: 5
    },
    bigIcon: {
      fontSize: 32
    }
});
