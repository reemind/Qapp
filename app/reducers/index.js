
import { combineReducers} from 'redux';
import PackagesReducer from './PackagesReducer';
import LoginSignUpReducer from './LoginSignUpReducer';
import ProfileReducer from './ProfileReducer';

import PasswordRecoveryReducer from './PasswordRecoveryReducer';


export default combineReducers({
    packagesdata: PackagesReducer,

    loginsignupuser:LoginSignUpReducer,
    passwordrecover:PasswordRecoveryReducer,
    profile:ProfileReducer
});
