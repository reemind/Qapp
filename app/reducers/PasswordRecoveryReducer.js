import {
    POST_RECOVERY_EMAIL,
    RECOVERY_EMAIL_VERIFICATION,
    RECOVERY_RESET_PASSWORD
} from '../actions/types';

const initialState = {
    post_recover_mail:{},
    recovery_email_verification_res:{},
    recover_new_password_res:{}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case POST_RECOVERY_EMAIL:
            return {
                ...state,
                post_recover_mail: action.payload
            };

            case RECOVERY_EMAIL_VERIFICATION:
            return {
                ...state,
                recovery_email_verification_res: action.payload
            };

            case RECOVERY_RESET_PASSWORD:
            return {
                ...state,
                recover_new_password_res: action.payload
            };

        default:
            return state;
    }
}
