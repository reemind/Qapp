import {
    FETCH_PACKAGES,
    FETCH_PACKAGE_DETAILS,
    NEW_POST
} from '../actions/types';

const initialState = {
    packages: [],
    packagedetails: [],
    item: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_PACKAGES:
            return {
                ...state,
                packages: action.payload
            };
            case FETCH_PACKAGE_DETAILS:
            return {
                ...state,
                packagedetails: action.payload
            };
        case NEW_POST:
            return {
                ...state,
                item: action.payload
            };
        default:
            return state;
    }
}
