import {
    FETCH_PROFILE_DATA,
    POST_PROFILE_DATA
} from '../actions/types';

const initialState = {
    profiledata: {},
    postProfiledata:{},
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_PROFILE_DATA:
            return {
                ...state,
                profiledata: action.payload
            };
       case POST_PROFILE_DATA:
            return {
                ...state,
                postProfiledata: action.payload
            };
       
        default:
            return state;
    }
}
