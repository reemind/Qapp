import {
    New_User_SignUp,
    POST_CREDENTIAL
} from '../actions/types';

const initialState = {

    newusersignup:{}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case New_User_SignUp:
            return {
                ...state,
                newusersignup: action.payload
            };

            case POST_CREDENTIAL:
            return {
                ...state,
                usercredential: action.payload
            };
            

        default:
            return state;
    }
}
