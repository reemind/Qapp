
import React, { Component } from "react";
import EStyleSheet from 'react-native-extended-stylesheet';
// import Initial from './screens/login/Initial';
import RootStack from './app/config/routes';
import { Provider } from 'react-redux';
import store from './app/store/store';

EStyleSheet.build({
    // $outline: 1,
    $primaryBlue: '#00a8a8',
    $primaryDarkBlue: '#017171',
    $primaryGray: 'gray',
    $lightGray: '#E4E4E4',
    $pinkColor: '#ec407a'
});
import { View, ScrollView, StyleSheet, StatusBar } from 'react-native';
import { PrimaryButton, StatusBarBackground, Logo } from './app/components/common/index';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content,Text, Card, CardItem } from "native-base";

export default class app extends Component {
  constructor(props) {
    super(props);
    this.state = {isReady: false};
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("native-base/Fonts/Ionicons.ttf")
    });
    this.setState({ isReady: true });
  }

  render() {
    return (
        <Provider store = {store} >
        <RootStack />
        </Provider>
    );
  }

}
